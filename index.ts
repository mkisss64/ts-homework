import {IUsers, IUsersNEW} from "./app/Interface/IUsers";
import {usersInfoArray} from "./userInfo";
import {usersArray} from "./users";

function getUsersJobPositions(usersArray: IUsers[]): IUsersNEW [] {
    const NewUsersArray: IUsersNEW[]=[];
    if (Array.isArray(usersArray)){
        usersArray.forEach((usersItem)=>{
            const userInfo = usersInfoArray.find(item=>item.userid === usersItem.userid);

            if(userInfo!=null){
                const UsersNew: IUsersNEW = {
                    name: usersItem.name,
                    position: userInfo.organization.position,
                    age: userInfo.age,
                    gender: usersItem.gender
                }
                NewUsersArray.push(UsersNew);
            }
        })
    }
    return NewUsersArray;

}
const usersPositions = getUsersJobPositions(usersArray);
console.log('userPositions', usersPositions);
