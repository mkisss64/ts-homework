export interface IUsersInfo {
    userid: string,
    name: string,
    birthdate: string,
    age: number,
    organization: IOrganization
};

export interface IOrganization {
    name: string,
    position: string
}

