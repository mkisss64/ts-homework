export interface IUsers {
    userid: string,
    name: string,
    gender: string
};

export interface IUsersNEW {
    name: string,
    position: string,
    age: number,
    gender: string
}